class Tile {
  constructor(x, y, type="None") {
    this.x = x;
    this.y = y;
    this.type = type;
  }

  change_type(building_type) {
    this.type = building_type;
  }
}

class Grid {
  constructor(height, width) {
    this.w = width;
    this.h = height;
    this.rows = [];

    for(let i = 0; i < this.h; i++) {
      var objects = [];
      for(let j = 0; j < this.w; j++) {
        objects.push(new Tile(i, j));
      }
      this.rows.push(objects);
    }

  }

  get_grid() {
    var temp = new Grid(this.h, this.w);

    for(let i = 0; i < this.h; i++) {
      for(let j = 0; j < this.w; j++) {
        temp.rows[i][j] = new Tile(i, j, this.rows[i][j].type);
      }
    }

    return temp;
  }

  change_building(x, y, building_type) {
    this.rows[x][y].change_type(building_type);
  }

  add_grid_row() {
    this.h++;
    var objects = [];

    for(let j = 0; j < this.w; j++) {
      objects.push(new Tile((this.h - 1), j));
    }

    this.rows.push(objects);
  }

  delete_grid_row() {
    this.h--;
    this.rows.pop();
  }
}

class UndoPattern {
  constructor(waterfront) {
    this.front = waterfront;
  }
}

//GRADING: MANAGE
class StateHandler {
  constructor() {
    this.states_undo = [];
    this.states_redo = [];
    this.index = 0;
  }

  //GRADING: COMMAND
  save_state(waterfront) {
    if (this.index > 5) {
      this.states_undo.shift();
      this.index = 5;
    }

    this.states_undo.push(new UndoPattern(waterfront));
    this.index++;
  }

  //GRADING: ACTION
  undo_change() {
    if (this.index <= 1)
      return;

    let temp = this.states_undo[this.states_undo.length - 1];

    this.states_redo.push(temp);

    this.states_undo.pop();

    temp = this.states_undo[this.states_undo.length - 1];
    this.index--;

    return temp;
  }

  redo_change() {
    if (this.states_redo.length < 1)
      return;

    let temp = this.states_redo[this.states_redo.length - 1];
    this.save_state(temp.front);
    this.states_redo.pop();

    return temp;
  }
}

var state = new StateHandler(); //State object for waterfront grid
var front_state; //Variable to hold waterfront grid
const storage = window["sessionStorage"]; //Variable to hold grid object in session

//GRADING: ACTION
function undo() {
  //Undo last action
  let old_state = state.undo_change();

  //Update model to last state
  front_state = old_state.front.get_grid();

  //Update view
  display_grid();
}

//GRADING: ACTION
function redo() {
  //Redo last action
  let old_state = state.redo_change();

  //Update model to last state
  front_state = old_state.front.get_grid();

  //Update view
  display_grid();
}

function change_tile(id, choice) {
  //Change Model
  front_state.change_building((front_state.h - 1), id, choice);

  //Save Change
  let old_state = front_state.get_grid();
  state.save_state(old_state);
  storage.setItem("curr_front", JSON.stringify(old_state));

  //Change View
  display_grid();
}

function delete_row() {
  //Change Model

  if(front_state.h == 1) {
    alert("Table already at min size!");
  }
  else {
    front_state.delete_grid_row();

    //Save Change
    let old_state = front_state.get_grid();
    state.save_state(old_state);
    storage.setItem("curr_front", JSON.stringify(old_state));

    //Change View
    display_grid();
  }
}

//Add model editing
function add_row() {

  //Max size of 5x5 grid
  if(front_state.h > 4) {
    alert("Table already at max size!");
  }
  else {

    //Change Model
    front_state.add_grid_row();

    //Save change
    let old_state = front_state.get_grid();
    state.save_state(old_state);
    storage.setItem("curr_front", JSON.stringify(old_state));

    //Change View
    display_grid();
  }
}

function reset_table() {

  //Change Model
  front_state = new Grid(1, 5);

  //Save change
  let old_state = front_state.get_grid();
  state.save_state(old_state);
  storage.setItem("curr_front", JSON.stringify(old_state));

  //Change view
  display_grid();
}

function all_hotels_table() {
  //Change Model
  front_state = new Grid(1, 5);

  for(let i = 0; i < 5; i++) {
    front_state.change_building((front_state.h - 1), i, "Hotel");
  }

  //Save change
  let old_state = front_state.get_grid();
  state.save_state(old_state);
  storage.setItem("curr_front", JSON.stringify(old_state));

  //Change view
  display_grid();
}

function mixed_table() {
  //Change Model
  front_state = new Grid(2, 5);
  front_state.change_building(0, 0, "House");
  front_state.change_building(0, 1, "Hotel");
  front_state.change_building(0, 2, "Hotel");
  front_state.change_building(0, 3, "Hotel");
  front_state.change_building(0, 4, "Hotel");
  front_state.change_building(1, 0, "House");
  front_state.change_building(1, 1, "Hotel");
  front_state.change_building(1, 2, "Hotel");
  front_state.change_building(1, 3, "House");
  front_state.change_building(1, 4, "None");

  //Save change
  let old_state = front_state.get_grid();
  state.save_state(old_state);
  storage.setItem("curr_front", JSON.stringify(old_state));

  //Change view
  display_grid();
}

function display_grid() {

  let table = document.getElementById("waterfront_grid");
  let length = table.rows.length;

  //Clear past html for grid
  if(length > 1) {
    for(let i = 1; i < length; i++) {
      document.getElementById("row" + (i - 1)).outerHTML = "";
    }
  }

  for(let i = 0; i < front_state.h; i++) {
    let row = table.insertRow();
    row.id = "row" + i;

    for(let j = 0; j < front_state.w; j++) {
      let col = row.insertCell();
      col.setAttribute("class", "building_tile");
      col.id = i + "_" + j;
      col.innerHTML = front_state.rows[i][j].type;
    }
  }
}

$(document).ready(function() {
  front_state = new Grid(1, 5);

  //Display current waterfront
  display_grid();

  let arg = window.location.search.substr(1).split("=");
  if(arg.length > 1) {
    const url = "https://dev.cse.sdsmt.edu/~s7522998/Waterfront/PHP/load_file.php?load_file=" + arg[1];
    fetch(url).then((resp) => {
      return resp.json();
      }).then((data) => {
      front_state = new Grid(data.data.h, data.data.w);
      data.data.rows.forEach((row)=> {row.forEach((e) => {front_state.change_building(e.x, e.y, e.type)})});
      display_grid();
      let old_state = front_state.get_grid();
      state.save_state(old_state);
      storage.setItem("curr_front", JSON.stringify(old_state));
    });
    //Check if a waterfront is available in session storage
  } else if(storage.getItem("curr_front")) {
    //If a front is saved, copy it
    let front_json = JSON.parse(storage.getItem("curr_front"));
    front_state = new Grid(front_json.h, front_json.w);

    front_json.rows.forEach((row)=> {row.forEach((e) => {front_state.change_building(e.x, e.y, e.type)})});
    display_grid();
    let old_state = front_state.get_grid();
    state.save_state(old_state);
    storage.setItem("curr_front", JSON.stringify(old_state));
  } else {
    //Save initial state of waterfront
    let old_state = front_state.get_grid();
    state.save_state(old_state);
    storage.setItem("curr_front", JSON.stringify(old_state));
  }

  //Event Handlers
  document.getElementById("col_0_button").addEventListener("click", function() {
    let choice = $("#col_0").find(":selected").text();
    change_tile(0, choice);
  });
  document.getElementById("col_1_button").addEventListener("click", function() {
    let choice = $("#col_1").find(":selected").text();
    change_tile(1, choice);
  });
  document.getElementById("col_2_button").addEventListener("click", function() {
    let choice = $("#col_2").find(":selected").text();
    change_tile(2, choice);
  });
  document.getElementById("col_3_button").addEventListener("click", function() {
    let choice = $("#col_3").find(":selected").text();
    change_tile(3, choice);
  });
  document.getElementById("col_4_button").addEventListener("click", function() {
    let choice = $("#col_4").find(":selected").text();
    change_tile(4, choice);
  });

  document.getElementById("reset").addEventListener("click", reset_table);
  document.getElementById("all_hotels").addEventListener("click", all_hotels_table);
  document.getElementById("mixed").addEventListener("click", mixed_table);

  document.getElementById("add_row").addEventListener("click", add_row);
  document.getElementById("delete_row").addEventListener("click", delete_row);
  document.getElementById("undo").addEventListener("click", undo);
  document.getElementById("redo").addEventListener("click", redo);
});
