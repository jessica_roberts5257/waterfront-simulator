# Waterfront Simulator

## AUTHOR:
Jessica Roberts

## DESCRIPTION:
The purpose of this program to simulate a waterfront property planner. Users can add rows of available properties to the waterfront and edit what type of building the properties become between a house, a hotel, or an empty lot. There is also a choice between three preconfigured arrangements, an all hotel row, two mixed rows, and an empty row. All changes can be undone or redone up to 5 times each.

## HOW TO USE:
Open the main.html file in any web browser. The main page contains the core functionality of the application. On this page, is the waterfront simulation area where property layouts can be created. The add row button adds a row of empty properties underneath the previous (closest to water line) row to create up to five rows. Delete row removes the last (furthest from water line) row of buildings but only if there is more than one row remaining. The undo and redo buttons undo and redo the most recent changes made to the waterfront up to five times, respectively. The select boxes underneath the main content are used to edit only the buildings in the last row. Each box corresponds to the building in the column directly above it. The reset button removes any changes that have been made and creates a single row of empty buildings. The all hotels button creates a preconfigured grid of one row where each building is a hotel. Lastly, the mixed button creates a preconfigured grid of two rows with a mix of hotels, houses, and empty lots. This page will save all changes to the simulation area when switching pages or reloading the page.